from django.urls import path

from pocapp.views import VueExemple

app_name = 'pocapp'
urlpatterns = [
    path('exemple/', VueExemple.as_view(), name='exemple'),
]

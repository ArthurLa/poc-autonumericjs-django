from django import forms

from django.forms import widgets


from pocapp.models import MaClasseAvecDesNombres


class FormMaClasseAvecDesNombres(forms.ModelForm):
    class Meta:
        model = MaClasseAvecDesNombres
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(FormMaClasseAvecDesNombres, self).__init__(*args, **kwargs)

        self.fields['nombre1'].widget = widgets.TextInput()
        self.fields['nombre2'].widget = widgets.TextInput()

        # Gestion des champs nombre
        champs_texte_decimal = ['nombre1', 'nombre2']
        champs_readonly = ['nombre2']

        # Ajout des classes + champs obligatoire.
        for (nom, champ) in self.fields.items():
            if nom in champs_texte_decimal:
                self.fields[nom].widget.attrs['class'] = 'form-control decimal2'

            if nom in champs_readonly:
                self.fields[nom].widget.attrs['readonly'] = True

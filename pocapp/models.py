from decimal import Decimal

from django.core.validators import MinValueValidator
from django.db import models


class MaClasseAvecDesNombres(models.Model):
    nombre1 = models.DecimalField(decimal_places=2, max_digits=12, validators=[MinValueValidator(Decimal('0.00'))])
    nombre2 = models.DecimalField(decimal_places=2, max_digits=12, validators=[MinValueValidator(Decimal('0.00'))])

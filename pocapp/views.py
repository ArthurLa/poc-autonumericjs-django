from django.views.generic import TemplateView

from pocapp.forms import FormMaClasseAvecDesNombres
from pocapp.models import MaClasseAvecDesNombres


class VueExemple(TemplateView):
    template_name = 'pocapp/exemple.html'

    def get_context_data(self, form=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['mes_nombres'] = MaClasseAvecDesNombres.objects.all()
        context['form'] = form if form else FormMaClasseAvecDesNombres()
        return context

    def post(self, request, **kwargs):
        form = FormMaClasseAvecDesNombres(request.POST)
        context = self.get_context_data(form=form, **kwargs)

        if not form.is_valid():
            return self.render_to_response(context, status=400)

        form.save()

        return self.render_to_response(context, status=201)
